Hooks.on('renderSidebarTab', (app, html) => {
    // Add a button to the sidebar
    html.find('.folder-footer').before(`
      <button class="chat-button">Open Chat</button>
    `);
  
    // Handle button clicks
    html.on('click', '.chat-button', () => {
      // Open a window with a chatbox
      new Dialog({
        title: 'Chat',
        content: `
          <div class="chat-container">
            <div class="chat-messages"></div>
            <input type="text" class="chat-input" placeholder="Enter a message..."></input>
          </div>
        `,
        buttons: {
          send: {
            label: "Send",
            callback: (html) => {
              // Send the message
              let message = html.find('.chat-input').val();
              let messages = html.find('.chat-messages');
              messages.append(`<div class="chat-message">${message}</div>`);
              html.find('.chat-input').val('');
            }
          }
        },
        default: "send"
      }).render(true);
    });
  });
  